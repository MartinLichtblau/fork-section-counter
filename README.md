[![Build Status](https://api.shippable.com/projects/538898fa5e755afa02b58514/badge/master)](https://www.shippable.com/projects/538898fa5e755afa02b58514)

Section Counter
==================
A Hypereton module
------------------


The section counter module parses a text-only pdf document containing laws and/or regulations pertaining to the changing of other laws and/or regulations. It extracts from those documents their number of first and second level sections.


### Parsing scheme
The parsing of the documents adheres to the following scheme:

- Every occurrence of quoted text is ignored to avoid confusing the revising legal text with the revisioned legal text.
- All the descriptors are considered valid only if located at the very beginning of a new paragraph and only if not followed by other text.
    This is in order to make sure that only stand-alone headers but not text sections or table of contents entries are being taken into account for the purposes of section counting.
- Only complete legal texts are being considered, i.e. text sections lacking title descriptors are ignored to prevent the counting of context free sections.
- Any legal text implicitly or explicitly marked as duplicate will not be taken into account for the purposes of section counting.
    

##### _Title descriptors_
There are numerous valid title descriptors. They start with 
"Gesetz" or "Verordnung" as either their first or second word and are 
followed at some point by the word "Änderung".
Composite words such as "Bildungsgesetz" are possible, too.

##### _Section descriptors_
There are two first level section descriptors - "Artikel " or "§ ", both followed by an arbitrary length arabic numeral.

##### _Subsection descriptor_
There is only one valid second level section descriptor - an arbitrary length arabic numeral followed by ". ".