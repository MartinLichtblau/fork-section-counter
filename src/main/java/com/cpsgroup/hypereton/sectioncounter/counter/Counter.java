package com.cpsgroup.hypereton.sectioncounter.counter;

import com.cpsgroup.hypereton.sectioncounter.decomposer.TextDecomposer;

/**
 * Provides a method to extract from a {@link String} containing a compilation
 * of legal texts the number of legal texts, and respective sections and
 * subsections it contains.<br>
 * Those features are extracted according to descriptors matching the respective
 * structural document levels.<br>
 * The descriptors make use of regular expressions, see
 * {@link java.util.regex.Pattern} for further information on syntax and usage.
 * 
 * 
 * @author Manuel Weidmann
 */
public class Counter {

	private TextDecomposer decomposer;

	/**
	 * Creates a new {@link Counter} to extract from a compilation of legal
	 * texts the number of legal texts, and respective sections and subsections
	 * it contains.
	 * 
	 * @param titleDescriptor
	 *            A regular expression {@link String} denoting the beginning of
	 *            a legal text.
	 * @param closureDescriptor
	 *            A regular expression {@link String} denoting the closing
	 *            comments of a legal text.
	 * @param sectionDescriptor
	 *            A regular expression {@link String} denoting the beginning of
	 *            a section in a legal text.
	 * @param subsectionDescriptor
	 *            A regular expression {@link String} denoting the beginning of
	 *            a subsection in a legal text.
	 * 
	 * @author Manuel Weidmann
	 */
	public Counter(String titleDescriptor, String closureDescriptor,
			String sectionDescriptor, String subsectionDescriptor) {
		decomposer = new TextDecomposer(titleDescriptor, closureDescriptor,
				sectionDescriptor, subsectionDescriptor);

	}

	/**
	 * Returns the number of legal texts, sections and subsections contained in
	 * this legal text compilation <br>
	 * <b>Note</b>: Legal texts and sections that are not divided any further
	 * count as having one section or subsection, respectively. The total number
	 * of individual legal statements is thus equal to the number of
	 * subsections.
	 * 
	 * @param legalTextCompilation
	 *            An HTML-formatted {@link String} containing a compilation of
	 *            legal texts.
	 * @return An {@link int} array containing the number of respective
	 *         structure level elements found in the compilation of legal texts.
	 * 
	 * @author Manuel Weidmann
	 */
	public int[] getTextStructure(String legalTextCompilation) {

		int numberOfLegalTexts = 0;
		int numberOfSections = 0;
		int numberOfSubsections = 0;

		/**
		 * Structure the string according to the descriptors.
		 */
		String[][][] decomposedText = decomposer
				.getDecomposedText(legalTextCompilation);

		/**
		 * First level contains legal texts, i.e. arrays of sections.<br>
		 * Second level contains sections, i.e. arrays of subsections.<br>
		 * Third level contains subsections, i.e. arrays of text.
		 */
		numberOfLegalTexts = decomposedText.length;

		for (String[][] legalText : decomposedText) {
			numberOfSections += legalText.length;
			for (String[] section : legalText) {
				numberOfSubsections += section.length;
			}
		}
		return new int[] { numberOfLegalTexts, numberOfSections,
				numberOfSubsections };

	}
}
