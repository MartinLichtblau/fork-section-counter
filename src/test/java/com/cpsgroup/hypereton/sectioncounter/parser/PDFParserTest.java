package com.cpsgroup.hypereton.sectioncounter.parser;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;

import org.junit.Before;
import org.junit.Test;

public class PDFParserTest {

	private final String PATH_PREFIX = "src" + File.separator + "test"
			+ File.separator + "resources" + File.separator + "parser"
			+ File.separator;
	private PDFParser parser;

	@Before
	public void init() {
		parser = new PDFParser();
	}

	/*
	 * Test file access
	 */
	@Test
	public void testNoReadAccess() {
		File file;
		FileChannel channel;
		FileLock lock = null;

		try {
			file = new File(PATH_PREFIX + "noReadAccess.pdf");

			channel = new RandomAccessFile(file, "rw").getChannel();
			lock = channel.lock();

			parser.getContent(PATH_PREFIX + "noReadAccess.pdf", 1, 1, true);
			fail("Should have thrown an IOException due to no read access.");

		} catch (OverlappingFileLockException e) {
			System.out
					.println("OverlappingFileLockException thrown as expected, all is fine here.");
		} catch (Exception e) {
			fail("Should have thrown an OverlappingFileLockException, threw "
					+ e.getClass() + " instead.");
			e.printStackTrace();
		}

		if (lock instanceof FileLock) {
			try {
				lock.release();
			} catch (IOException e) {
			}
		}
	}

	@Test
	public void testValidLocation() {
		try {
			parser.getContent(PATH_PREFIX + "readAccess.pdf", 1, 1, true);
		} catch (IOException e) {
			fail("Should not have failed accessing file.");
			e.printStackTrace();
		}
	}

	@Test
	public void testInvalidLocation() {
		try {
			parser.getContent(PATH_PREFIX + "readAccessInvalidLocation.pdf", 1,
					1, true);
			fail("File does not exist, should have failed accessing.");
		} catch (IOException e) {
			System.out
					.println("IOException thrown as expected, all is fine here.");
		}
	}

	@Test
	public void testNotPDF() {
		try {
			parser.getContent(PATH_PREFIX + "readAccess.txt", 1, 1, true);
			fail("File is not a PDF file, should have failed loading in as PDF.");
		} catch (IOException e) {
			System.out
					.println("IOException thrown as expected, all is fine here.");
		}
	}

	/**
	 * 
	 * Checking for encryptiong does not work as of yet due to pdfbox
	 * dependencies not resolving as they should.
	 * 
	 * @author Manuel Weidmann
	 */
	/**
	 * TODO: Resolve PDFbox/bouncycastle dependency issue in Maven build!
	 */
	/*
	 * @Test public void encryptedPDF() { try { parser.getContent(PATH_PREFIX +
	 * "encrypted.pdf", 1, 1, true);
	 * fail("File is encrypted, should have failed extraction."); } catch
	 * (IOException e) { } }
	 */

	@Test
	public void corruptedPDF() {
		try {
			parser.getContent(PATH_PREFIX + "corrupted.pdf", 1, 1, true);
			fail("File is corrupted, should have failed extraction.");
		} catch (IOException e) {
			System.out
					.println("IOException thrown as expected, all is fine here.");
		}
	}

	/*
	 * Test quotation handling
	 */

	@Test
	public void testProperSingleQuote() {
		try {
			assertFalse(parser.getContent(PATH_PREFIX + "properSingle.pdf", 1,
					1, true).contains("some text"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void testProperMultiQuote() {
		try {
			assertFalse(parser.getContent(PATH_PREFIX + "properSingle.pdf", 1,
					1, true).contains("some text"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void testSolitaryClosingQuoteMarks() {
		try {
			String content = parser.getContent(PATH_PREFIX
					+ "solitaryClosing.pdf", 1, 1, true);
			assertTrue("Expected content to contain 'some text' but was "
					+ content, content.contains("some text"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void testSolitaryClosingQuoteMarksMulti() {
		try {
			String content = parser.getContent(PATH_PREFIX
					+ "solitaryClosingMulti.pdf", 1, 1, true);
			assertFalse("Expected content not to contain 'some text' but was "
					+ content, content.contains("some text"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void testQuotesNotRemovedOption() {
		try {
			assertTrue(parser.getContent(PATH_PREFIX + "properSingle.pdf", 1,
					1, false).contains("some text"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	/*
	 * Test range specification handling
	 */
	@Test
	public void testStartingGreaterThanEnding() {
		try {
			String content = parser.getContent(PATH_PREFIX + "oneToFive.pdf",
					3, 2, false);
			assertTrue(content.contains("page one"));
			assertTrue(content.contains("page two"));
			assertTrue(content.contains("page three"));
			assertTrue(content.contains("page four"));
			assertTrue(content.contains("page five"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void testStartLessThan1() {
		try {
			String content = parser.getContent(PATH_PREFIX + "oneToFive.pdf",
					0, 2, false);
			assertTrue(content.contains("page one"));
			assertTrue(content.contains("page two"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void testEndingGreaterThanLength() {
		try {
			String content = parser.getContent(PATH_PREFIX + "oneToFive.pdf",
					3, 10, false);
			assertTrue(content.contains("page three"));
			assertTrue(content.contains("page four"));
			assertTrue(content.contains("page five"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void properRange() {
		try {
			String content = parser.getContent(PATH_PREFIX + "oneToFive.pdf",
					2, 4, false);
			assertTrue(content.contains("page two"));
			assertTrue(content.contains("page three"));
			assertTrue(content.contains("page four"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

	@Test
	public void properSinglePage() {
		try {
			String content = parser.getContent(PATH_PREFIX + "oneToFive.pdf",
					3, 3, false);
			assertTrue(content.contains("page three"));
		} catch (IOException e) {
			fail("Exception thrown.");
		}
	}

}
